# Le pâté et la tarte

Ce projet prend le nom d'[une farce du moyen
age](https://fr.wikipedia.org/wiki/Le_P%C3%A2t%C3%A9_et_la_Tarte), sans réelle
rapport, si ce n'est que l'objet du dit projet est de dénoncer une farce autour
du nombre d'emploi à pourvoir dans le numérique...

Bien que la formation dans ce domaine soit certainement à améliorer, je crois
qu'il n'est pas nécessaire de dire que tout le monde doit apprendre la
programmation, et qu'il y aura du travail pour tous.

Le but est pour moi de faire apparaitre le nombre d'offres d'emplois pour
junior.


## Référence à lire

- http://recrute.pole-emploi.fr/page-entreprise/futur-emploi
- https://syntec-numerique.fr/formation-emploi-mixite
- https://www.lesechos.fr/idees-debats/cercle/cercle-133197-le-numerique-un-secteur-qui-ne-connait-pas-la-crise-1124166.php
- http://www.latribune.fr/actualites/economie/france/20141208tribc0b40518d/pour-pole-emploi-le-saut-dans-le-numerique-est-indispensable-jean-basseres.html
- https://cnnumerique.fr/wp-content/uploads/2015/12/Rapport-travail-version-finale-janv2016.pdf
- http://www.usine-digitale.fr/numerique/
- https://www.pole-emploi.org/clubrh/connexion-membres-@/clubrh/727/view-category-32009.html?
- http://www.regionsjob.com/actualites/recrutements-numerique.html
- https://www.lesechos.fr/idees-debats/cercle/cercle-158072-le-numerique-menace-ou-opportunite-pour-le-service-public-de-lemploi-2007462.php
- http://www.terrafemina.com/emploi-a-carrieres/vie-travail/articles/25797-top-10-des-metiers-du-futur-devenez-clapotiseur-numeropathe-ou-murateur-.html
- https://digital-society-forum.orange.com/fr/les-forums/700-le_numerique_lremploi_a_reinventern
- http://www.regionsjob.com/actualites/900-000-emplois-a-saisir-dans-le-numerique.html
- http://www.studyrama-emploi.com/home_interview.php?id=6791

